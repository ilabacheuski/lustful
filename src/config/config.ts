import { window, workspace } from 'vscode'
import ProviderController from '../providers/providerController'

export class Config {
  readonly providerController: ProviderController = new ProviderController()
  constructor() {}

  async init(): Promise<void> {
    const providersSettings = await this.loadSettings()
    if (!providersSettings) return
    const providerNames = Object.keys(providersSettings)
    if (!providerNames.length) return
    for (const name of providerNames) {
      providersSettings[name].forEach(options => {
        try {
          this.providerController.loadProvider(name, options)
        } catch (e) {
          console.log(e) // Add better handling
        }
      })
    }
  }

  async loadSettings(): Promise<IProviderSettings | undefined> {
    return workspace.getConfiguration('lustfulReviewer').get('providers') as IProviderSettings
  }

  async saveSettings(settings: IProviderSettings): Promise<void> {
    const workspaceConfig = await workspace.getConfiguration('lustfulReviewer')
    return await workspaceConfig.update('providers', settings, true)
  }

  async addProviderSettings(provider: IProvider): Promise<void> {
    const providerSettings = provider.getProviderSettings()
    const settings = await this.loadSettings()
    for (const name of Object.keys(providerSettings)) {
      const providerOptions = providerSettings[name]
      let options = settings[name] || []
      const ind = provider.indexOfInOptions(options)
      if (ind === -1) {
        options = options.concat(providerOptions)
      } else {
        options[ind] = providerOptions
      }
      settings[name] = options
    }
    await this.saveSettings(settings)
  }

  async addProvider(): Promise<void> {
    const providerNames = this.providerController.getProviderNames()
    if (!providerNames.length) {
      window.showErrorMessage(`No registered providers! Ask contributors for help`)
      return
    }
    const name = await window.showQuickPick(providerNames, { placeHolder: 'Choose SCM provider' })
    if (!name) return
    try {
      const provider = await config.providerController.createProvider(name)
      await this.addProviderSettings(provider)
      window.showInformationMessage(provider.getSuccessCreationMessage())
    } catch (e) {
      window.showErrorMessage((<Error>e).message)
    }
  }
}

let config: Config

export default async function getConfig(): Promise<Config> {
  if (config) return config
  config = new Config()
  await config.init()
  return config
}
