import BitbucketProvider from './bitbucketProvider'

export default class ProvidersController {
  private _providers: IProvider[] = []
  get providers(): IProvider[] {
    return this._providers
  }
  private _registeredProviders: IRegisteredProviderDefinition = {}
  get registeredProviders(): IRegisteredProviderDefinition {
    return this._registeredProviders
  }

  constructor() {
    this._registeredProviders = {
      bitbucket: {
        name: BitbucketProvider.ProviderId,
        useClass: BitbucketProvider,
        description: 'Default bitbucket provider',
      },
    }
  }

  registerProvider(name: string, definition: IProviderDefinition) {
    this._registeredProviders[name] = definition
  }

  unregisterProvider(name: string) {
    delete this._registeredProviders[name]
  }

  getProviderNames() {
    return Object.keys(this._registeredProviders)
  }

  getConstructor(name: string): IProviderConstructor {
    const definition = this._registeredProviders[name]
    if (!definition) throw new Error(`There is no such provider: ${name}`)
    return definition.useClass
  }

  addProvider(provider: IProvider) {
    const isProvider = this._providers.find(el => el.id === provider.id)
    if (isProvider) {
      throw new Error(`Provider with ${provider.id} already exist`)
    }
    this.providers.push(provider)
  }

  loadProvider(name: string, opts?: IProviderSettingsOptions): IProvider {
    const ProviderConstructor = this.getConstructor(name)
    const provider = new ProviderConstructor(opts)
    this.addProvider(provider)
    return provider
  }

  removeProvider(provider: IProvider) {
    this._providers = this._providers.reduce((acc, el) => {
      if (el !== provider) {
        acc.push(el)
      }
      return acc
    }, [])
  }

  async createProvider(name: string): Promise<IProvider> {
    const ProviderConstructor = this.getConstructor(name)
    const provider = await ProviderConstructor.create()
    this.addProvider(provider)
    return provider
  }
}
