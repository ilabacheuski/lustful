import { window, workspace } from 'vscode'

interface IBitbucketProvider extends IProvider {
  readonly token: string
}

interface IBitbucketOptions extends IProviderConstructorOptions {
  token: string
}

const PROVIDER_NAME = 'bitbucket'

export default class BitbucketProvider implements IBitbucketProvider {
  static ProviderId = PROVIDER_NAME
  static async create(): Promise<IBitbucketProvider | null> {
    const token = await window.showInputBox({ placeHolder: `Token for: ${PROVIDER_NAME}` })
    if (!token) return null
    return new BitbucketProvider({ token })
  }

  readonly id: string
  readonly name: string = PROVIDER_NAME
  private _repositories: IRepository[]
  get repositories(): IRepository[] {
    return this._repositories
  }
  description: string

  private _token: string
  get token(): string {
    return this._token
  }

  constructor({ token, repositories = [], description = '' }: IBitbucketOptions) {
    this._token = token
    this._repositories = repositories
    this.description = description
    this.id = token
  }

  getSuccessCreationMessage() {
    return `Provider ${this.name} was added with token ${this.token}`
  }

  update({ token, repositories = [], description = '' }: IBitbucketOptions) {
    if (this.id !== token) {
      throw new Error(`The id: ${this.id} of this provider should equal token: ${token}`)
    }
    this._token = token
    this._repositories = repositories
    this.description = description
  }

  setToken(token: string) {
    this._token = token
  }

  async getRepositories(): Promise<IRepository[]> {
    return []
  }

  getProviderSettings(): IProviderSettings {
    return {
      [PROVIDER_NAME]: [
        {
          token: this.token,
        },
      ],
    }
  }

  indexOfInOptions(options: IProviderSettingsOptions[]): number {
    return options.findIndex(el => el.token === this._token)
  }
}
