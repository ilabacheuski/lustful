interface IProviderSettings {
  [name: string]: IProviderSettingsOptions[]
}

interface IProviderSettingsOptions {
  [name: string]: any
}

interface IProviderConstructorOptions {
  description?: string
  repositories?: IRepository[]
}

interface IProvider {
  readonly id: string
  readonly name: string
  repositories: IRepository[]
  description?: string
  update: (IProviderConstructorOptions) => void
  setToken: (string) => void
  getRepositories: () => Promise<IRepository[]>
  getProviderSettings: () => IProviderSettings
  getSuccessCreationMessage: () => string
  indexOfInOptions: (options: IProviderSettingsOptions[]) => number
}

interface IRepository {
  name: string
  url: string
}

interface IProviderConstructor {
  new (opts?: Object): IProvider
  ProviderId: string
  create: () => Promise<IProvider | null>
}

interface IProviderDefinition {
  name: string
  useClass: IProviderConstructor
  description?: string
}

interface IRegisteredProviderDefinition {
  [providerName: string]: IProviderDefinition
}
