import { Command, TreeItem, TreeItemCollapsibleState } from 'vscode'

export class NodeToken extends TreeItem {
  constructor(
    public readonly label: string,
    public readonly collapsibleState: TreeItemCollapsibleState = TreeItemCollapsibleState.None,
    public readonly command?: Command
  ) {
    super(label, collapsibleState)
    this.command = { title: 'Add token', command: 'lustfulReviewer.addProvider' }
  }

  // contextValue = 'provider'

  getTreeItem(): TreeItem {
    return {
      label: this.label,
      collapsibleState: this.collapsibleState,
      command: this.command,
    }
  }

  async getChildren(element): Promise<NodeToken[]> {
    return []
  }
}
