import {
  commands,
  Event,
  EventEmitter,
  TreeDataProvider,
  TreeItem,
  TreeItemCollapsibleState,
  Uri,
  window,
} from 'vscode'
import { Config } from '../config/config'
import { NodeToken } from './models/nodeToken'

export default class Explorer implements TreeDataProvider<TreeItem> {
  private _onDidChangeTreeData: EventEmitter<TreeItem | null> = new EventEmitter<TreeItem | null>()
  onDidChangeTreeData?: Event<TreeItem> = this._onDidChangeTreeData.event

  rootNodes: NodeToken[]

  constructor(private config: Config) {
    // this.rootNodes = config.providerController.providers
  }

  getTreeItem(element: TreeItem): TreeItem | Promise<TreeItem> {
    return element
  }
  getChildren(element?: TreeItem): TreeItem[] | Promise<TreeItem[]> {
    // if (!this.config) {
    const nodeToken = new NodeToken('Add token')
    return Promise.resolve([nodeToken])
    // }
  }

  refresh(): void {
    this._onDidChangeTreeData.fire()
  }
}
