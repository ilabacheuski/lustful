import { commands, window, workspace, QuickPickOptions } from 'vscode'
import getConfig from '../config/config'

export default function registerCommands(): void {
  commands.registerCommand('lustfulReviewer.addProvider', async () => {
    const config = await getConfig()
    config.addProvider()
  })
}
