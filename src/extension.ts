'use strict'
import * as vscode from 'vscode'
import registerCommands from './commands/commands'
import getConfig from './config/config'
import Explorer from './explorer/explorer'

export async function activate(context: vscode.ExtensionContext) {
  try {
    const config = await getConfig()
    const reviewerExplorer = new Explorer(config)
    vscode.window.registerTreeDataProvider('lustfulReviewerExplorer', reviewerExplorer)
    registerCommands()
  } catch (e) {
    vscode.window.showErrorMessage('Lustful Reviewer failed to load')
  }
}

export function deactivate() {}
